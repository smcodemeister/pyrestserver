from http.server import HTTPServer, BaseHTTPRequestHandler
from io import BytesIO
import json

server_address = 'localhost'
server_port = 8080

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'GET Triggered')

    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        body = self.rfile.read(content_length)
        self.send_response(200)
        self.end_headers()
        response = BytesIO()
        #response.write(b'This is POST request. ')
        #response.write(b'Received: ')
        response.write(body)
        self.wfile.write(response.getvalue())   
        jsonBody = json.loads(body)
        print("--------------------------------")
        print(jsonBody)     
        print("--------------------------------")
        print("Data received - waiting for next data set ....")


httpd = HTTPServer((server_address, server_port), SimpleHTTPRequestHandler)
print("Server running - waiting for data ....")
httpd.serve_forever()
